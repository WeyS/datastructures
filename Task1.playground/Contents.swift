import UIKit

struct Prototype<Key : Hashable, Value> {
    var values: [(Key, Value)] = []
}

class MyDictionary<Key : Hashable , Value> {
    var storage = Array<Prototype<Key, Value>>(repeating: Prototype<Key, Value>(), count: 2)
    
    private let max = 0.6
    
    private var size: Int {
        return storage.count
    }
      
    private var count = 0
      
    private var currentLoad: Double {
        return Double(count) / Double(size)
      }
    
    func resize() {
        if currentLoad > max {
            let plusStorage = Array<Prototype<Key, Value>>(repeating: Prototype<Key, Value>(), count: size)
            storage += plusStorage
        }
    }
    
    func append (key: Key, value: Value) {
        resize()
        let position = abs(key.hashValue) % size
        storage[position].values.append((key, value))
        count += 1
    }
    
    func value (key: Key) -> Value? {
        let position = abs(key.hashValue) % size
        return storage[position].values.first {$0.0 == key}?.1
    }
    
    func all() -> [[(key: Key, value: Value)]]  {
        let x = storage.map { (res) -> [(key: Key, value: Value)] in
            res.values
        }
        return x
    }
    
}

var dictionary = MyDictionary<Int, Int>()

dictionary.append(key: 1, value: 8)
dictionary.append(key: 2, value: 4)
dictionary.value(key: 1)
print(dictionary.all())



struct Prototype1<Value : Hashable> {
    var values: [Value] = []
}



class MySet<Value : Hashable>{
    var storage = Array<Prototype1<Value>>(repeating: Prototype1<Value>(), count: 2)
    
    private let max = 0.6
    
    private var size: Int {
        return storage.count
    }
      
    private var count = 0
      
    private var currentLoad: Double {
        return Double(count) / Double(size)
      }
    
    func resize() {
        if currentLoad > max {
            let plusStorage = Array<Prototype1<Value>>(repeating: Prototype1<Value>(), count: size)
            storage += plusStorage
        }
    }

    func append(value: Value){
        resize()
        let position = abs(value.hashValue) % size
        if storage[position].values.contains(value){
            return
        }else {
            storage[position].values.append(value)
            count += 1
        }
    }

    func contains(value: Value) -> Bool{
        let position = abs(value.hashValue) % size
        return storage[position].values.contains{$0 == value}
    }
    

    func all() -> [[Value]]{
        let x = storage.map { (res) -> [Value] in
            res.values
        }
        return x
    }
}

let set = MySet<Int>()

set.append(value: 4)
set.append(value: 4)
set.append(value: 5)
set.contains(value: 5)
print(set.all())
