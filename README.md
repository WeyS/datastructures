# DataStructures

Implemented my own classes MyDictionary and MySet using HashTable.

MyDictionary<Key, Value>
  - append(key: Key, value: Value)
  - value(for: Key) -> Value 
  - all() -> [(key: Key, value: Value)] - array of tuples

MySet<Value>
  - append(value: Value)
  - all() -> [Value]
  - contains(value: Value) -> Bool
